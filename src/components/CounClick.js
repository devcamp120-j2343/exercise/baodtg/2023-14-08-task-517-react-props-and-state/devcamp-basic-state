const { Component } = require("react");

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }
        // C1: sử dụng bind trỏ this của function về this của class.
        // this.clickChangeHanlder = this.clickChangeHanlder.bind(this)
    }
    // clickChangeHanlder() {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }
    clickChangeHanlder = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    render() {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChangeHanlder}>Click here</button>
            </div>
        )
    }
}
export default CountClick